using UnityEngine;
using System.Collections.Generic;

public class Abilities : MonoBehaviour
{
    [SerializeField]
    private List<UnitAbility> mAbilitiesList = new List<UnitAbility>();

    private void OnEnable()
    {
        List<UnitAbility> tempAbilities = new List<UnitAbility>();
        foreach (UnitAbility ability in mAbilitiesList)
        {
            UnitAbility abil = Instantiate(ability) as UnitAbility;
            tempAbilities.Add(abil);
        }

        mAbilitiesList.Clear();
        mAbilitiesList = tempAbilities;
    }
    /// <summary>
    /// Returns the ability given the index
    /// will return the last ability in the list if index is greater than the size
    /// </summary>
    /// <param name="index"> the slot # of the ability list </param>
    /// <returns> the UnitAbility specified </returns>
    public UnitAbility GetAbility(int index)
    {
        return (index >= mAbilitiesList.Count) ? mAbilitiesList[mAbilitiesList.Count - 1] : mAbilitiesList[index];
    }

    public bool SetAbility(int index, UnitAbility ability)
    {
        return mAbilitiesList[index] = ability;
    }
}
