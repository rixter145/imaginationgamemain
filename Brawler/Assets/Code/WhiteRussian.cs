using UnityEngine;
using System.Collections;

public class WhiteRussian: MonoBehaviour {
	
	private float timeSpentAliveLen = 2f;
    private float moveSpeed = 5f;
    private float timeSpentAlive = 0f;
	private Vector3 direction;
	public int Damage = 0;
    public GameObject objAttacker = null;
	private HealthScript hs;
	
	// Use this for initialization
	void Start () {
		direction = Vector3.right;
	}
	
	// Update is called once per frame
	void Update()
    {
        timeSpentAlive += Time.deltaTime;
        if (timeSpentAlive > timeSpentAliveLen) // if we have been traveling for more than .1 seconds remove the bullet
        {
            RemoveMe();
        }
        else
        {
            transform.Translate(direction * moveSpeed * Time.deltaTime);
         
        }
    }
	
	/* When the gameObject (ie. the bullet) collides, check whether the object that was collided with has a HealthScript attached. If it does, decrease its 'curHP' by 'damage' */

    public void OnTriggerEnter(Collider col)
    {
        hs = col.gameObject.GetComponent<HealthScript>();
        if (hs != null)
        {
            hs.HurtHealth(Damage, objAttacker);
            //EventSystem.Instance.Post(new CombatEvent(this.gameObject, Damage));
        }

    }

    private void RemoveMe()
    {
        // handle animation, event firing etc. here
        Destroy(gameObject);
    }
}
