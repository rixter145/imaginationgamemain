using UnityEngine;
using System.Collections;

public class EnemyAttackAbility : UnitAbility
{
    public GameObject AbilityPrefab = null;

    private Vector3 currentPos = Vector3.zero;
    private Vector3 facingVector = Vector3.zero;
    private Collider selfCollider = null;

    private GameObject objAttacker = null;

    protected override void OnEnable()
    {
        base.OnEnable();
        if (AbilityPrefab == null)
        {
            Debug.LogError("No Ability Prefab set for  " + this);
        }
    }

    /* Create a temp GameObject for its collider for collision detection */

    public override void UseEffect()
    {
        if (CoolDownFinished())
        {
            StartCoolDown();
            Vector3 offset = facingVector * .75f;
            GameObject temp = (GameObject)Instantiate(AbilityPrefab);
            DamageOthers dmgScript = temp.GetComponent<DamageOthers>();
            if (dmgScript == null)
            {
                Debug.LogError("Cannot find Damage Script within Ability prefab for ability " + this);
            }
            else
            {
                dmgScript.Damage = DamageAmount;
                dmgScript.SetRotation(facingVector);
                dmgScript.objAttacker = objAttacker;
            }
            temp.transform.position = currentPos + offset;
            Physics.IgnoreCollision(temp.collider, selfCollider);
        }
    }
	

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
        facingVector = ev.AbilityFacingFec;
        selfCollider = ev.SrcCollider;
        objAttacker = ev.AbilitySource;
    }
}
