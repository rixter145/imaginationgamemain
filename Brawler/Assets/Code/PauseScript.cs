using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {
	
	private bool paused = false;
	
	private Abilities abilManager;
	private int  selectedAbilityIdx = -1;
	
	// We use these to keep track of time while timeScale == 0
	private float PreviousRealTime = 0.0f;
	private float PausedDeltaTime = 0.0f;
	
	// Use to keep track of when we're transitioning/animating the loadout menu in
	private bool IsInTransition = false;
	private int initGUISpawnHeight = 20;
	private float deltaTransition = 0.0f;
	private float distanceTransitioned = 0.0f;
	
	// Game Object usage
	private GameObject player;
	public GameObject GUI_BGPrefab = null;
	private GameObject GUIBackground = null;
	
	public UnitAbility[] SkillsAvailable;
	
	
	// We're going to switch from using the pause to
	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
		abilManager = player.GetComponent<Abilities>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		PausedDeltaTime = Time.realtimeSinceStartup - PreviousRealTime;
		PreviousRealTime = Time.realtimeSinceStartup;
		
		if (Input.GetButtonDown ("Pause"))
		{
			HandlePause();
			IsInTransition = true;
		}
		
		if (paused && IsInTransition)
		{
			// Transition into pause screen
			HandleLoadoutAnimation(true);
		}
		else if (!paused && IsInTransition)
		{
			// Transition out of pause screen
			HandleLoadoutAnimation(false);
		}
	}
	
	void OnGUI()
	{
		if (Time.timeScale == 0.0f && paused && !IsInTransition)
		{
			GUI.Box(new Rect(Screen.width/2 - 25, Screen.height/4 - 15, 50, 30), "Paused");
			
			if (GUI.Button(new Rect(Screen.width*2.5f/8 - 60, Screen.height/3, 120, 50), abilManager.GetAbility(1).abilityName))
			{
				selectedAbilityIdx = 1;
			}
			if (GUI.Button(new Rect(Screen.width*4/8 - 60, Screen.height/3, 120, 50), abilManager.GetAbility(2).abilityName))
			{
				selectedAbilityIdx = 2;
			}
			if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height/3, 120, 50), abilManager.GetAbility(3).abilityName))
			{
				selectedAbilityIdx = 3;
			}
			
			// Ability selection
			if (selectedAbilityIdx > 0)
			{
				if (GUI.Button(new Rect(Screen.width*2.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[0].abilityName))	
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[0]);	// ability at index 0 is always AttackAbility, so you're changing the other 3 abilities
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*4/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[1].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[1]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[2].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[2]);
					selectedAbilityIdx = 0;
				}
				
				// I guess we can't use For loops to setup draw code for OnGUI - that's a bummer
				/*
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[3].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[3]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[4].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[4]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[5].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[5]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[6].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[6]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[7].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[7]);
					selectedAbilityIdx = 0;
				}
				if (GUI.Button(new Rect(Screen.width*5.5f/8 - 60, Screen.height*5/8, 120, 50), SkillsAvailable[8].abilityName))
				{
					abilManager.SetAbility(selectedAbilityIdx, SkillsAvailable[8]);
					selectedAbilityIdx = 0;
				}
				 */
				
			}
		}	
	}
	
	void HandlePause()
	{
		paused = !paused;
		Time.timeScale = paused ? 0.0f : 1.0f;
		distanceTransitioned = 0.0f;
		if (paused)
		{
			// Clear out any old instances of the BG
			if (GUIBackground)
			{
				Destroy(GUIBackground);
			}
			
			// Instantiate a new one above the screen to transition in
			GUIBackground = (GameObject) Instantiate(GUI_BGPrefab, new Vector3(0, 10, initGUISpawnHeight), Quaternion.identity);
		}
		
	}
	
	void HandleLoadoutAnimation(bool TransitioningIn)
	{
		if (GUIBackground)
		{
			// Check to see if our distanced traveled is == to the total transition translation
			if (distanceTransitioned <= (float) initGUISpawnHeight)
			{
					deltaTransition = 50.0f*PausedDeltaTime;
					GUIBackground.transform.position += TransitioningIn ? new Vector3(0, 0, -deltaTransition) : new Vector3(0, 0, deltaTransition);
					distanceTransitioned += deltaTransition;
			}
			else
			{
				// We're at the end of the transition
				IsInTransition = false;
				distanceTransitioned = 0.0f;
				if (!TransitioningIn)
				{
					Destroy(GUIBackground);	
				}
			}
		}
	}
	
}
