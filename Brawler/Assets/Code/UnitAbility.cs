using UnityEngine;
using System.Collections;
using Events;

public abstract class UnitAbility : ScriptableObject
{
    #region Designer Variables
    public int DamageAmount = 0;
    public int HealingAmount = 0;
    public float CoolDownLength = 0f;
	public string abilityName = null;
    #endregion

    #region Art HookUps
    public Transform ArtAnimation = null;
    public Transform AbilityArtButton = null;
    #endregion

    #region Private Variables
    private float m_coolDownFinishedTime = 0f;
    #endregion

    #region Component Methods
    public UnitAbility()
    {

    }

    protected virtual void OnEnable()
    {
        m_coolDownFinishedTime = 0f;	//need to do this to get rid of past values
        EventSystem.Instance.Register<AbilityEvent>(ObtainCharacterInformation);
    }

    public void StartCoolDown()
    {
        m_coolDownFinishedTime = Time.time + CoolDownLength;
    }

    public bool CoolDownFinished()
    {
        return Time.time >= m_coolDownFinishedTime;
    }
    #endregion

    #region Abstract Methods
    public abstract void UseEffect();
    protected abstract void ObtainCharacterInformation(AbilityEvent ev);
    #endregion


}
