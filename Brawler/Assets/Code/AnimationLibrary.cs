using UnityEngine;
using System.Collections;

public class AnimationLibrary : MonoBehaviour
{


    /* 	Definitions
         * 
         * colSize : # of total columns in the sprite sheet
         * rowSize : # of total rows in the sprite sheet
         * colFrameStart : Indicates which frame # to start in the x axis of the sprite sheet - STARTS AT 0
         * rowFrameStart : Indicates which frame # to start in the y axis of the sprite sheet - STARTS AT 0
         * totalFrames : The total number of frames in the animation along the x-axis
         * fps : The Framerate (frames per second)
         * 
    */

    public void AnimateSprite(int colSize, int rowSize, int colFrameStart, int rowFrameStart, int totalFrames, int fps)
    {

        // Calculate the index as a factor off of time
        int index = (int)(Time.time * fps);
        index = index % totalFrames;

        // Calculate individual sprite offsets & u/v coordinates
        Vector2 size = new Vector2(1.0f / colSize, 1.0f / rowSize);
        float u = index % colSize;
        float v = index / colSize;

        // Use the above information to set the offset vector
        Vector2 offset = new Vector2((u + colFrameStart) * size.x, (1.0f - size.y) - (v + rowFrameStart) * size.y);

        //Debug.Log("frame: " + index + " u: " + u + " v: " + v + " offsetX: " + offset.x + " offsetY: " + offset.y);

        // Apply the size and offset vectors to the material texture (ie. Sprite sheet)
        renderer.material.mainTextureOffset = offset;
        renderer.material.mainTextureScale = size;

        renderer.material.SetTextureOffset("_MainTex", offset);
        renderer.material.SetTextureScale("_MainTex", size);

    }


    /*
     *  Overloaded method that allows for calling animations on a gameObject other than the one that this
     *  script is currently attached to.
     */

    public void AnimateSprite(GameObject obj, int colSize, int rowSize, int colFrameStart, int rowFrameStart, int totalFrames, int fps)
    {

        // Calculate the index as a factor off of time
        int index = (int)(Time.time * fps);
        index = index % totalFrames;

        // Calculate individual sprite offsets & u/v coordinates
        Vector2 size = new Vector2(1.0f / colSize, 1.0f / rowSize);
        float u = index % colSize;
        float v = index / colSize;

        // Use the above information to set the offset vector
        Vector2 offset = new Vector2((u + colFrameStart) * size.x, (1.0f - size.y) - (v + rowFrameStart) * size.y);

        //Debug.Log("frame: " + index + " u: " + u + " v: " + v + " offsetX: " + offset.x + " offsetY: " + offset.y);

        // Apply the size and offset vectors to the material texture (ie. Sprite sheet)
        obj.renderer.material.mainTextureOffset = offset;
        obj.renderer.material.mainTextureScale = size;

        obj.renderer.material.SetTextureOffset("_MainTex", offset);
        obj.renderer.material.SetTextureScale("_MainTex", size);

    }

    /** FADE
     * 
     *  Helper method for fading in/out gameobjects
     *  NOTE: This operates on the object's material, the material
     * 		  MUST be using a shader that has an adjustable alpha value, a.
     * 
     **/

    public IEnumerator Fade(float alph_from, float alph_to, float duration)
    {
        float startTime = Time.time;
        Color colour = renderer.material.color;
        while (Time.time - startTime < duration)
        {
            colour.a = Mathf.Lerp(alph_from, alph_to, (Time.time - startTime) / duration);
            renderer.material.color = colour;
            yield return 0;
        }

    }

}
