using UnityEngine;
using System.Collections;

public class TeddyTerror : MonoBehaviour {
	
	private Vector3 inputMovement;
	public float moveSpeed = 2f;
	public CharacterController controller;
	private Vector3 updatedVector;
	private GameObject objPlayer;
	private float dispersionTime = .5f;
	private float timeToLive = 10f;
	public int Damage = 0;
	public GameObject objAttacker = null;
	
	// Use this for initialization
	void Start () {
		objPlayer = GameObject.FindWithTag("Player");	//TODO
		dispersionTime += Time.time;
		timeToLive += Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > timeToLive){
			Explode();
		}
		FindAIInput();
		ProcessMovement();
	}
	
	private void ProcessMovement(){
        inputMovement = transform.TransformDirection(inputMovement);
        controller.Move(inputMovement.normalized * Time.deltaTime * moveSpeed);
		transform.position = new Vector3(transform.position.x,1,transform.position.z);	//locks the object to y=0
    }
	
	private void FindAIInput(){
		if(Time.time < dispersionTime){
			inputMovement = FleeAI();
		}
		else{
   	    	inputMovement = RandomAI();
		}
    }
	
	private Vector3 FleeAI(){
		return transform.position - objPlayer.transform.position;
	}
	
	//Goes around randomly (2% chance of changing direction)
	private Vector3 RandomAI(){
		if (Random.value > .98f){
			updatedVector = new Vector3(Random.Range(-10.0f,10.0f),0,Random.Range(-10.0f,10.0f));
			return updatedVector;
		}
		else
			return updatedVector;
	}
	
	private void Explode(){
		Destroy(gameObject);
	}
}
