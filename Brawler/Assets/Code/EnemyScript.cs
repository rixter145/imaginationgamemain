using UnityEngine;
using System.Collections;

using Events;

public enum AI
{
    HomingAI,
    FleeAI,
    AttackAI,
    IdleAI,
    RangedAI,
}

public class EnemyScript : MonoBehaviour
{

    public AI currentAI;
    public float moveSpeed = 2f;
    public float gravity = 20f;
    public Abilities EnemyAbilities = null;
    public float attackRadius = 1.1f;
    public int maxHp;

    private Vector3 facingVec;
    private GameObject objPlayer;
    private Vector3 inputMovement;
    private CharacterController controller;

    public bool isStunned = false;
    private AI lastAI;

    [HideInInspector]
    [SerializeField]
    private float distanceBetweenPlayer = 0f;
    [HideInInspector]
    [SerializeField]
    private float zDistanceThreshold = 0f;


    // Use this for initialization
    void Start()
    {
        objPlayer = GameObject.FindWithTag("Player");
        EventSystem.Instance.Register<CombatEvent>(DeTarget);
        controller = GetComponent<CharacterController>();

        if (controller == null)
        {
            Debug.LogError("Cannot find character controller for AI, AI will not move!");
        }
    }

    private void DeTarget(CombatEvent evt)
    {
        // TODO: Find another target
        if (evt.IsDead)
            objPlayer = null;
    }

    // Update is called once per frame
    void Update()
    {
        FindAIInput();
        ProcessMovement();
        HandleFacing();
    }

    private void FindAIInput()
    {
        if (objPlayer == null || isStunned)
        {
            currentAI = AI.IdleAI;
        }
        switch (currentAI)
        {
            case AI.HomingAI:
                inputMovement = HomingAI();
                break;
            case AI.FleeAI:
                inputMovement = FleeAI();
                break;
            case AI.IdleAI:
                inputMovement = IdleAI();
                break;
            case AI.RangedAI:
                inputMovement = RangedAI();
                break;
        }
    }

    private void ProcessMovement()
    {
        inputMovement = transform.TransformDirection(inputMovement);
        inputMovement.y -= gravity * Time.deltaTime;	//apply gravity
        controller.Move(inputMovement.normalized * Time.deltaTime * moveSpeed);
    }

    /* Chases after the player in a straight line and attacks when close enough (attackRadius)*/
    private Vector3 HomingAI()
    {
        if (Vector3.Distance(objPlayer.transform.position, transform.position) <= attackRadius)
        {
            Attack();
            return Vector3.zero;
        }
        else
        {
            return objPlayer.transform.position - transform.position;
        }
    }

    private Vector3 RangedAI()
    {
        float dist = (objPlayer.transform.position.x - transform.position.x) * (objPlayer.transform.position.x - transform.position.x);
        if (dist <= distanceBetweenPlayer * distanceBetweenPlayer)
        {
            return transform.position - objPlayer.transform.position;
        }
        else
        {
            float zDist = (objPlayer.transform.position.z - transform.position.z) * (objPlayer.transform.position.z - transform.position.z);
            if (zDist <= zDistanceThreshold)
            {
                Attack();
            }
            else
            {
                return new Vector3(0, 0, objPlayer.transform.position.z - transform.position.z);
            }
        }

        return Vector3.zero;
    }

    private Vector3 FleeAI()
    {
        return transform.position - objPlayer.transform.position;
    }

    private Vector3 IdleAI()
    {
        return Vector3.zero;
    }


    private void Attack()
    {
        // just copied from playerscript
        EnemyAttackAbility ability = EnemyAbilities.GetAbility(0) as EnemyAttackAbility;
        if (ability != null)
        {
            if (facingVec == Vector3.zero)
            {
                HandleFacing();
            }
            EventSystem.Instance.Post(new AbilityEvent(this.transform.position, this.facingVec, this.collider, gameObject));
            ability.UseEffect();
        }

    }

    private void HandleFacing()
    {
        if (objPlayer != null)
        {
            if (objPlayer.transform.position.x - transform.position.x > 0)
            {
                facingVec = new Vector3(1, 0, 0);
            }
            else
            {
                facingVec = new Vector3(-1, 0, 0);
            }
        }
    }

    /* Stuns the GameObject by setting the AI to idleAI for duration seconds
     * TODO: Make a buff/debuff system so this won't be required */

    public IEnumerator Stun(float duration)
    {
        lastAI = currentAI;
        isStunned = true;
        yield return new WaitForSeconds(duration);
        isStunned = false;
        currentAI = lastAI;
    }


}
