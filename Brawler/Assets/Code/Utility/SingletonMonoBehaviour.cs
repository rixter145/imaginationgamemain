using UnityEngine;
using System;

namespace Utility
{
    public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        private static bool _searchedForInstance = false;

        public static T Instance
        {
            get
            {
                if (_instance == null && !_searchedForInstance)
                {
                    _instance = SearchInstance();
                }
                return _instance;
            }
        }

        private static T SearchInstance()
        {
            T[] InstancesOfT = (T[])UnityEngine.Object.FindObjectsOfType(typeof(T));

            if (InstancesOfT == null || InstancesOfT.Length == 0)
            {
                Debug.LogError("Game does not contain an instance of " + typeof(T).Name);
            }
            else if (InstancesOfT.Length > 1)
            {
                Debug.LogError("Game contains multiple instances of " + typeof(T).Name);
            }

            _searchedForInstance = true;
            return InstancesOfT[0];
        }
    }
}