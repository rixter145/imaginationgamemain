using UnityEngine;
using System.Collections;

public class WhiteRussianAbility : UnitAbility {

	private Vector3 currentPos = Vector3.zero;
    private GameObject objAttacker = null;
	public GameObject AbilityPrefab;
	private Collider selfCollider = null;
	
    public override void UseEffect()
    {
		if (CoolDownFinished())
        {
			Debug.Log("White Russian");
            StartCoolDown();
            Vector3 offset = Vector3.left * 2f;
            GameObject temp = (GameObject)Instantiate(AbilityPrefab);
            WhiteRussian dmgScript = temp.GetComponent<WhiteRussian>();
            if (dmgScript == null)
            {
                Debug.LogError("Cannot find Damage Script within Ability prefab for ability " + this);
            }
            else
            {
                dmgScript.Damage = DamageAmount;
                dmgScript.objAttacker = objAttacker;
            }
            temp.transform.position = currentPos + offset;
            Physics.IgnoreCollision(temp.collider, selfCollider);
        }
	}

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
		selfCollider = ev.SrcCollider;
        objAttacker = ev.AbilitySource;
    }
}
