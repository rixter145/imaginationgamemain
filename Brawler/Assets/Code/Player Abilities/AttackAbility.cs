using UnityEngine;
using System.Collections;

public class AttackAbility : UnitAbility
{
    public GameObject AbilityPrefab = null;

    private Vector3 currentPos = Vector3.zero;
    private Vector3 facingVector = Vector3.zero;
    private Collider selfCollider = null;
    private int mComboCount = 0;
	private GameObject objAttacker = null;

    private float mComboEndTime = float.MaxValue;
    private float mComboFallOffLen = .75f;

    protected override void OnEnable()
    {
        base.OnEnable();
        mComboCount = 0;
        mComboEndTime = float.MaxValue;
    }

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
        facingVector = ev.AbilityFacingFec;
        selfCollider = ev.SrcCollider;
		objAttacker = ev.AbilitySource;
    }
	
	/* Create a temp GameObject for its collider for collision detection */
	
    public override void UseEffect()
    {
        if (CoolDownFinished())
        {
            StartCoolDown();
			HandleCombo();
            Vector3 offset = facingVector * .5f;
            GameObject temp = (GameObject)Instantiate(AbilityPrefab);
            DamageOthers dmgScript = temp.GetComponent<DamageOthers>();
            if (dmgScript == null)
            {
                Debug.LogError("Cannot find Damage Script within Ability prefab for ability " + this);
            }
            else
            {
                dmgScript.Damage = DamageAmount;
                dmgScript.SetRotation(facingVector);
                dmgScript.objAttacker = objAttacker;
            }
            temp.transform.position = currentPos + offset;
            Physics.IgnoreCollision(temp.collider, selfCollider);
        }
    }
	
	/* Fires a 'COMBO' on every third attack within mComboFallOfflen seconds */
	
    private void HandleCombo()
    {
        if (Time.time <= mComboEndTime)
        {
            mComboEndTime = Time.time + mComboFallOffLen;
            mComboCount++;
            if (mComboCount >= 3)
            {
                Debug.Log("COMBO");
                mComboCount = 0;
            }
        }
        else
        {
            mComboCount = 1;
            mComboEndTime = Time.time + mComboFallOffLen;
        }
    }
}
