using UnityEngine;
using System.Collections;

public class FinalFlightAbility : UnitAbility
{
    private PlayerScript ps;

    public override void UseEffect()
    {
        Debug.Log("Final Flight");
        ps = GameObject.FindWithTag("Player").GetComponent<PlayerScript>(); // TODO: Change this to getter/setter
        CoRoutineStarter.Instance.StartCoroutine(ps.BackFlip());
    }

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {

    }
}
