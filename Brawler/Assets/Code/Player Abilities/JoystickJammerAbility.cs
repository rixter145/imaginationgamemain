using UnityEngine;
using System.Collections;

public class JoystickJammerAbility : UnitAbility
{

    private Vector3 currentPos = Vector3.zero;
    public float abilityRadius = 0.0f;
    private EnemyScript es;
    public float stunDuration = 0f;

    public override void UseEffect()
    {
        Debug.Log("Joystick Jammer");
        Collider[] targets = Physics.OverlapSphere(currentPos, abilityRadius);
        foreach (Collider col in targets)
        {
            es = col.gameObject.GetComponent<EnemyScript>();
            if (es != null)
            {
                CoRoutineStarter.Instance.StartCoroutine(es.Stun(stunDuration));
            }
        }
    }

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
    }
}
