using UnityEngine;
using System.Collections;

using Events;

public class HealthScript : MonoBehaviour
{

    public int curHp;
    public float invulnLen = 1f;	// length of on-hit invulnerability

    public bool isReflect = false;

    private float mEndTime = 0f;
    public PlayerScript ps;
    public EnemyScript es;

    private HealthScript ahs;   // attacker's HealthScript
    public GameObject objAttacker = null;

    // Use this for initialization
    void Start()
    {
        if (ps != null) { curHp = ps.maxHp; }
        else if (es != null) { curHp = es.maxHp; }
    }

    // Update is called once per frame
    void Update()
    {

    }

    /* Hurts the player for 'amount' and checks if the player dies. If not, make the player invulnerable 
     * -on-hit invulnerability was decided to be disabled 
     * -will there be knockback?*/

    public void HurtHealth(int amount, GameObject attacker)
    {
		//Debug.Log(amount +" "+ attacker);
        if(ps!=null && isReflect){
            ahs = attacker.GetComponent<HealthScript>();
            ahs.HurtHealth(amount, attacker);
        }
        else if (Time.time >= mEndTime) // check if the player is already invulnerable
        {
            mEndTime = Time.time + invulnLen;
            curHp = curHp - amount;
            if (curHp <= 0)
            {
                HandleDeath();
            }

        }
    }


    private void HandleDeath()
    {
        // handle animation, event firing etc. here
        print(gameObject + " died");
        if (ps != null)
        {
            EventSystem.Instance.Post(new CombatEvent(ps, true));
        }
        else
        {
            EventSystem.Instance.Post(new CombatEvent(es, true));
        }
        Destroy(gameObject);
    }
}
