using UnityEngine;
using System.Collections;

public class MedievalMadnessAbility : UnitAbility
{

    private HealthScript hs;
    public float refectDuration = 0;

    public override void UseEffect()
    {   // TODO: knock enemies down
        Debug.Log("Medieval Madness");
        hs = GameObject.FindWithTag("Player").GetComponent<HealthScript>(); // TODO: Change this to getter/setter
        CoRoutineStarter.Instance.StartCoroutine(ReflectTimer(refectDuration));
    }

    private IEnumerator ReflectTimer(float duration)
    {
        hs.isReflect = true;
        yield return new WaitForSeconds(duration);
        hs.isReflect = false;
    }

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
     
    }
}
