using UnityEngine;
using System.Collections;

public class DanceFeverAbility : UnitAbility
{
    private Vector3 currentPos = Vector3.zero;
    public float mAbilityRadius = 0.0f;
    public int mAbilityDamage = 1;
    private GameObject objAttacker = null;
	
    public override void UseEffect()
    {
        Collider[] targets = Physics.OverlapSphere(currentPos, mAbilityRadius);
        foreach (Collider col in targets)
        {
            HealthScript hs = col.gameObject.GetComponent<HealthScript>();
			PlayerScript ps = col.gameObject.GetComponent<PlayerScript>();
            if (hs != null && ps == null)
            {
                hs.HurtHealth(mAbilityDamage, objAttacker);
            }
        }
    }

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
        objAttacker = ev.AbilitySource;
    }
}
