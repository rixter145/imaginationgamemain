using UnityEngine;
using System.Collections;

public class TeddyTerrorAbility : UnitAbility {

	//private Vector3 currentPos = Vector3.zero;
    private GameObject objAttacker = null;
	public GameObject AbilityPrefab;
	private Collider selfCollider = null;
	private GameObject objPlayer = null;
	
    public override void UseEffect()
    {
		if (CoolDownFinished())
        {
			Debug.Log("Teddy Terror");
            StartCoolDown();
			objPlayer = objAttacker;
            CoRoutineStarter.Instance.StartCoroutine(SpawnTeddy());
        }
	}

    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        //currentPos = ev.SrcPos;
		selfCollider = ev.SrcCollider;
        objAttacker = ev.AbilitySource;
    }
	
	private IEnumerator SpawnTeddy(){
		int i = 0;
		while(i <= 12){
			Vector3 offset = new Vector3(Random.value, 0, Random.value);
	        GameObject temp = (GameObject)Instantiate(AbilityPrefab);
	        TeddyTerror teddyScript = temp.GetComponent<TeddyTerror>();
	        if (teddyScript == null){
	        	Debug.LogError("Cannot find TeddyTerror Script within Ability prefab for ability " + this);
	        }
	        else{
	        	teddyScript.Damage = DamageAmount;
	        	teddyScript.objAttacker = objAttacker;
	        }
	        //temp.transform.position = currentPos + offset*.25f;	// TODO: this doesn't work since the event gets overwritten
			temp.transform.position = objPlayer.transform.position + offset*.5f;
	        Physics.IgnoreCollision(temp.collider, selfCollider);
			yield return new WaitForSeconds(.25f);
			i++;
		}
	}
}