using UnityEngine;
using System.Collections;

public class MahLazerAbility : UnitAbility
{
    public GameObject AbilityPrefab = null;

    private Vector3 currentPos = Vector3.zero;
    private Vector3 facingVector = Vector3.zero;
    private Collider selfCollider = null;

    public override void UseEffect()
    {
        Vector3 offset = facingVector * .5f;
        GameObject temp = (GameObject)Instantiate(AbilityPrefab);
        DamageOthers dmgScrp = temp.GetComponent<DamageOthers>();
        if (dmgScrp != null)
        {
            dmgScrp.Damage = DamageAmount;
        }
        temp.transform.position = currentPos + 10*offset;
        Physics.IgnoreCollision(temp.collider, selfCollider);
    }
	
	// Better plan of action:
	// Create a lazer beam prefab, that's responsible for it's own damage/transform?
	// Destroy that instance once the button is released - need an event for it
	
	
    protected override void ObtainCharacterInformation(Events.AbilityEvent ev)
    {
        currentPos = ev.SrcPos;
        facingVector = ev.AbilityFacingFec;
        selfCollider = ev.SrcCollider;
    }
}