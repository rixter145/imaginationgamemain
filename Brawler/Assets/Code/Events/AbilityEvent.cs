using UnityEngine;
using System.Collections;

namespace Events
{
    public class AbilityEvent : EventBase
    {
        public readonly Vector3 SrcPos = Vector3.zero;
        public readonly Vector3 AbilityFacingFec = Vector3.zero;
        public readonly Collider SrcCollider;
        public readonly GameObject AbilitySource;

        public AbilityEvent(Vector3 sourcePos, Vector3 facingVec, Collider selfCol, GameObject attacker)
        {
            SrcPos = sourcePos;
            AbilityFacingFec = facingVec;
            SrcCollider = selfCol;
            AbilitySource = attacker;
        }
       
    }
}