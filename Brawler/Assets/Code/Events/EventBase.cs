using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events
{
    public abstract class EventBase
    {
        public object Sender;
        public float Time;
    }

}
