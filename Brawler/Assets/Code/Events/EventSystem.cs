using Utility;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events
{
    public delegate void EventHandler<T>(T evt) where T : EventBase;

    public class EventSystem : Singleton<EventSystem>
    {
        public readonly Dictionary<Type, Delegate> dict;
        private readonly Dictionary<object, Delegate> lambdas;

        public EventSystem()
        {
            dict = new Dictionary<Type, Delegate>();
            lambdas = new Dictionary<object, Delegate>();
        }

        public void Register<T>(EventHandler<T> del) where T : EventBase
        {
            if (dict.ContainsKey(typeof (T)) && del != null)
                dict[typeof (T)] = Delegate.Combine(dict[typeof (T)], del);
            else
                dict.Add(typeof (T), del);
        }

        public void Register<T>(object sender, EventHandler<T> act) where T : EventBase
        {
            lambdas.Add(sender, act);
            Register(act);
        }

        public void Deregister<T>(object sender, EventHandler<T> act) where T : EventBase
        {
            Deregister(lambdas[sender] as EventHandler<T>);
        }

        public void Deregister<T>(EventHandler<T> del) where T : EventBase
        {
            if (dict.ContainsKey(typeof(T)))
            {
                dict[typeof (T)] = Delegate.Remove(dict[typeof (T)], del);
                if (dict[typeof (T)] == null)
                    dict.Remove(typeof (T));
            }
        }

        public void Post(EventBase evt)
        {
            if (evt != null && dict.ContainsKey(evt.GetType()))
                dict[evt.GetType()].DynamicInvoke(evt);
        }

    }
}