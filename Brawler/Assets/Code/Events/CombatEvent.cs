using UnityEngine;
using System.Collections;

namespace Events
{
    public class CombatEvent : EventBase
    {
        public readonly UnitAbility Ability = null;
        public readonly int Damage = 0;
        public readonly GameObject Target = null;
        public readonly bool IsDead = false;
        public readonly PlayerScript Player = null;
        public readonly EnemyScript Enemy = null;

        public CombatEvent(UnitAbility ability,  GameObject target, int damage)
        {
            Target = target;
            Ability= ability;
            Damage = damage;
        }

        public CombatEvent(GameObject target, int damage)
        {
            Target = target;
            Damage = damage;
        }

        public CombatEvent(PlayerScript player, bool death)
        {
            Player = player;
            IsDead = death;
        }

        public CombatEvent(EnemyScript enemy, bool death)
        {
            Enemy = enemy;
            IsDead = death;
        }
    }
}