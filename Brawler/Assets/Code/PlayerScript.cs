using UnityEngine;
using System.Collections;
using Events;

public class PlayerScript : MonoBehaviour
{

    public Vector3 inputMovement;
    CharacterController controller;
    private GameObject objCamera;
    public float jumpSpeed = 35f;
    public float gravity = 100f;
    public Vector3 facingVec;
    public Abilities PlayerAbilities = null;
    private Vector3 backflip;
    public bool isBackflip = false;

    // stats
    public float moveSpeed = 5f;
    public float moveMultiplier = 1f;    // for move speed changes
    public int maxHp = 10;
    public float dmgMultiplier = 1f; // for damage reduction (on player) - how do we deal with fractions of hp?

    private int mSprintCount = 0;
    private float mSprintEndTime = float.MaxValue;
    private float mSprintFallOffLen = .25f;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
        objCamera = GameObject.FindWithTag("MainCamera");
        facingVec = new Vector3(1, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //if(isFree){
        FindPlayerInput();
        //}
        HandleFacing();
        ProcessMovement();
        HandleCamera();
    }

    /* Finds player input including up, down, left, right and jumping. Movement is allowed while in the air */

    private void FindPlayerInput()
    {
        if (Input.GetButtonUp("Horizontal"))
        {
            moveMultiplier = 1f;
        }
        if (controller.isGrounded)
        {
            if (Input.GetButtonDown("Horizontal"))
            {
                HandleSprinting();
            }
            inputMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            if (Input.GetButton("Jump"))
            {
                inputMovement.y = jumpSpeed;

                // save test
                PlayerPrefs.SetInt("jumps", PlayerPrefs.GetInt("jumps") + 1);
                print("Total jumps: " + PlayerPrefs.GetInt("jumps"));
            }
        }

        else if (!controller.isGrounded) // allows movement while in the air
        {
            inputMovement.x = Input.GetAxis("Horizontal");
            inputMovement.z = Input.GetAxis("Vertical") * .65f;	// z movement should have a penalty while in the air
        }

        if (Input.anyKeyDown)
        {
            EventSystem.Instance.Post(new AbilityEvent(this.transform.position, this.facingVec, this.collider, gameObject));
        }

        if (Input.GetButtonDown("Fire1"))
        {
            UnitAbility ability = PlayerAbilities.GetAbility(0);
            if (ability != null)
            {
                ability.UseEffect();
            }
        }

        if (Input.GetButtonDown("Ability1"))
        {
            UnitAbility ability = PlayerAbilities.GetAbility(1);
            if (ability != null)
            {
                ability.UseEffect();
            }
        }

        if (Input.GetButtonDown("Ability2"))
        {
            UnitAbility ability = PlayerAbilities.GetAbility(2);
            if (ability != null)
            {
                ability.UseEffect();
            }
        }

        if (Input.GetButtonDown("Ability3"))
        {
            UnitAbility ability = PlayerAbilities.GetAbility(3);
            if (ability != null)
            {
                ability.UseEffect();
            }
        }

        inputMovement.y -= gravity * Time.deltaTime; // apply gravity
    }

    /* Checks whether the has player toggled sprinting by double tapping horizontal movement 
     * -currently it gives the sprint bonus even when changing directions as long as the movement is horizontal and the double tap is within mSprintEndTime */

    private void HandleSprinting()
    {
        if (Time.time <= mSprintEndTime)
        {
            mSprintEndTime = Time.time + mSprintFallOffLen;
            mSprintCount++;
            if (mSprintCount >= 2)
            {
                Debug.Log("SPRINT");
                moveMultiplier = 2f;
                mSprintCount = 0;
            }
        }
        else
        {
            mSprintCount = 1;
            mSprintEndTime = Time.time + mSprintFallOffLen;
        }
    }

    /* Moves and animates the player */

    private void ProcessMovement()
    {
        //controller.Move (inputMovement * Time.deltaTime * moveSpeed * moveMultiplier);

        // make moveSpeed and moveMultiplier only affect non-vertical movement (and jumpSpeed for vertical movement)
        inputMovement.x = inputMovement.x * moveSpeed * moveMultiplier;
        inputMovement.z = inputMovement.z * moveSpeed;	// sprinting movement bonus should not increase z movement
        if (!isBackflip)
        {
            controller.Move(inputMovement * Time.deltaTime);
        }
        else
        {

            backflip.y -= 85 * Time.deltaTime; // apply gravity
            backflip.x = facingVec.x * -600f * Time.deltaTime;
            controller.Move(backflip * Time.deltaTime);

        }
    }

    /* Moves the camera to follow the player left and right */

    private void HandleCamera()
    {
        objCamera.transform.position = new Vector3(transform.position.x,
                                                    objCamera.transform.position.y,
                                                    objCamera.transform.position.z);
    }

    /* Set the facing depending what the last movement was */

    private void HandleFacing()
    {
        if (inputMovement.x > 0)
        {
            facingVec = new Vector3(1, 0, 0);
        }
        if (inputMovement.x < 0)
        {
            facingVec = new Vector3(-1, 0, 0);
        }
    }

    public IEnumerator BackFlip()
    {  // TODO: knock enemies back
        isBackflip = true;
        if (controller.isGrounded)
        {
            backflip.y = 25f;
        }
        yield return new WaitForSeconds(.6f);    //wait until player lands
        isBackflip = false;
        //now apply knockback to the area the player lands
    }
}