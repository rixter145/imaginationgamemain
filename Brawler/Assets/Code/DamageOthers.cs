using UnityEngine;
using System.Collections;

using Events;

public class DamageOthers : MonoBehaviour
{
    public enum DamageType
    {
        Melee,
        Range
    }
    [HideInInspector]
    [SerializeField]
    private float timeSpentAliveLen = 5f;
    [HideInInspector]
    [SerializeField]
    private float moveSpeed = 0.5f;
    private float timeSpentAlive = 0f;
    private HealthScript hs;

    [HideInInspector]
    public int Damage = 0;
    public DamageType TypeofDamage = DamageType.Melee;
    public GameObject objAttacker = null;

    private Vector3 direction;

    public void SetRotation(Vector3 direction)
    {
        this.direction = direction;
    }

    void Update()
    {
        timeSpentAlive += Time.deltaTime;
        if (timeSpentAlive > timeSpentAliveLen) // if we have been traveling for more than .1 seconds remove the bullet
        {
            RemoveMe();
        }
        else
        {
            transform.Translate(direction * moveSpeed * Time.deltaTime);
         
        }
    }

    /* When the gameObject (ie. the bullet) collides, check whether the object that was collided with has a HealthScript attached. If it does, decrease its 'curHP' by 'damage' */

    public void OnCollisionEnter(Collision col)
    {
        hs = col.gameObject.GetComponent<HealthScript>();
        if (hs != null)
        {
            hs.HurtHealth(Damage, objAttacker);
            EventSystem.Instance.Post(new CombatEvent(this.gameObject, Damage));
            RemoveMe();
        }

    }

    private void RemoveMe()
    {
        // handle animation, event firing etc. here
        Destroy(gameObject);
    }
}
