using UnityEngine;
using UnityEditor;

public class CreateAbilityAsset
{
    [MenuItem("Assets/Create/UnitAbilities/AttackAbility")]
    public static void CreateAttackAsset()
    {
        ScriptableObjectUtility.CreateAsset<AttackAbility>();
    }
	
	[MenuItem("Assets/Create/UnitAbilities/DanceFeverAbility")]
    public static void CreateDanceFeverAblAsset()
    {
        ScriptableObjectUtility.CreateAsset<DanceFeverAbility>();
    }

    [MenuItem("Assets/Create/UnitAbilities/FinalFlightAbility")]
    public static void CreateFinalFlightAblAsset()
    {
        ScriptableObjectUtility.CreateAsset<FinalFlightAbility>();
    }

    [MenuItem("Assets/Create/UnitAbilities/JoystickJammerAbility")]
    public static void CreateJoystickJammerAblAsset()
    {
        ScriptableObjectUtility.CreateAsset<JoystickJammerAbility>();
    }

    [MenuItem("Assets/Create/UnitAbilities/MedievalMadnessAbility")]
    public static void CreateMedievalMadnessAblAsset()
    {
        ScriptableObjectUtility.CreateAsset<MedievalMadnessAbility>();
    }
	
	[MenuItem("Assets/Create/UnitAbilities/MahLazerAbility")]
    public static void CreateMahLazerAblAsset()
	{
		ScriptableObjectUtility.CreateAsset<MedievalMadnessAbility>();	
	}
	
	[MenuItem("Assets/Create/UnitAbilities/SpaceTrainAbility")]
    public static void CreateSpaceTrainAblAsset()
	{
		ScriptableObjectUtility.CreateAsset<SpaceTrainAbility>();	
	}
	
	[MenuItem("Assets/Create/UnitAbilities/WhiteRussianAbility")]
    public static void CreateWhiteRussianAblAsset()
	{
		ScriptableObjectUtility.CreateAsset<WhiteRussianAbility>();	
	}
	
	[MenuItem("Assets/Create/UnitAbilities/TeddyTerrorAbility")]
    public static void CreateTeddyTerrorAblAsset()
	{
		ScriptableObjectUtility.CreateAsset<TeddyTerrorAbility>();	
	}
}
