using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(EnemyScript))]
public class EnemyScriptEditor : Editor
{
    private SerializedProperty aiRange;
    private SerializedProperty zThreshold;

    private EnemyScript Target
    {
        get { return (EnemyScript)target; }
    }

    private void OnEnable()
    {
        aiRange = serializedObject.FindProperty("distanceBetweenPlayer");
        zThreshold = serializedObject.FindProperty("zDistanceThreshold");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        switch (Target.currentAI)
        {
            case AI.RangedAI:
                aiRange.floatValue = EditorGUILayout.FloatField("Distance Between Player", aiRange.floatValue);
                EditorGUILayout.LabelField("Projectile Maximum Difference Between Player");
                EditorGUILayout.BeginHorizontal();
                zThreshold.floatValue = EditorGUILayout.FloatField(zThreshold.floatValue);
                EditorGUILayout.EndHorizontal();
                break;
        }

        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(Target);
    }
}
