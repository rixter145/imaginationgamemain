using UnityEngine;
using System.Collections;

using UnityEditor;

[CustomEditor(typeof(DamageOthers))]
public class DamageOthersEditor : Editor
{
    private SerializedProperty timeSpentAliveLength;
    private SerializedProperty moveSpeed;

    private DamageOthers Target
    {
        get { return (DamageOthers)target; }
    }

    private void OnEnable()
    {
        timeSpentAliveLength = serializedObject.FindProperty("timeSpentAliveLen");
        moveSpeed = serializedObject.FindProperty("moveSpeed");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.BeginHorizontal();
        switch (Target.TypeofDamage)
        {
            case DamageOthers.DamageType.Melee:
            case DamageOthers.DamageType.Range:
               timeSpentAliveLength.floatValue = EditorGUILayout.FloatField("Prefab Alive Length", timeSpentAliveLength.floatValue);
                moveSpeed.floatValue = EditorGUILayout.FloatField("Prefab Move Speed", moveSpeed.floatValue);
                break;
        }
        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(Target);
    }
}
