using UnityEngine;
using System.Collections;

public class AIScript : MonoBehaviour 
{
	public float m_MoveSpeed = 20.0f; // speed he moves at
	public float m_AttackMoveSpeed = 10.0f; // attack speed
	public float m_JumpSpeed = 4.0f; // jump speed
	public float m_AttackRange = 1.0f;
	public float m_SearchRange = 2.0f;
	public float m_ReturnHomeRange = 4.0f;
	public float m_ChangeDirectionDistance = 2.0f;
	
	public int m_ColSize; // col size of the sprite sheet
	public int m_RowSize; // row size of sprite sheet
	public int m_ColFrameStart; // start index of frame col
	public int m_RowFrameStart; // start index of frame row
	public int m_TotalFrames; // total frames of the animation to run
	public int m_fps; // frames per second

	
	public Transform m_ChaseTarget; // Player 
	public Transform m_HomeTarget; // Spawn point
	
	public float m_DeathForce = 3.0f; // When player hits me, then go off by x amount
	
	public bool m_GizmoToggle = true; // Toggle the display of debug radius
	
	private Vector3 velocity = Vector3.zero; // store the velocity of enemy
	private float gravity = 20.0f; // keeps the enemy pushed down
	public AIState m_CurrentState = AIState.Idle;
	private charAnim aniPlay;
	private bool isRight = false;
	
	private float resetMoveSpeed = 0.0f;
	private float distanceToHome = 0.0f;
	private float distanceToTarget = 0.0f;
	private CharacterController controller;
	
	
	
	// Use this for initialization
	void Start () 
	{
		aniPlay = GetComponent<charAnim>();
		Application.runInBackground = true;
		resetMoveSpeed = m_MoveSpeed;
		controller = GetComponent<CharacterController>();
	//	aniPlay = GetComponent<AnimSprite>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		ProcessAI();
	}
	
	void ProcessAI()
	{
		if(m_CurrentState != AIState.EnemyDie)
		{
	
			distanceToTarget = Vector3.Distance(m_ChaseTarget.position,transform.position);
			
			if(distanceToTarget <= m_SearchRange)
			{
				StartCoroutine(randomJump());
				ChasePlayer();
				
				
				if(distanceToTarget <= m_AttackRange)
				{
					ChasePlayer();
					m_MoveSpeed = m_AttackMoveSpeed;
				}
				else
				{
					m_MoveSpeed = resetMoveSpeed;
					ChasePlayer();
				}
			}
			else
			{
				distanceToHome = Vector3.Distance(m_HomeTarget.position,transform.position);
				if(distanceToHome > m_ReturnHomeRange)
				{
					m_CurrentState = AIState.GoHome;
				}
			}
			
			if(controller.isGrounded)
			{
				switch(m_CurrentState)
				{
				case AIState.MoveLeft:
					PatrolLeft();
					break;
					
				case AIState.MoveRight:
					PatrolRight();
					break;
					
				case AIState.Idle:
					if(isRight)
					{ StopRight(); }
					else
					{ StopLeft(); }
					break;
				
					
				case AIState.Jump:
					if(isRight)
					{ JumpRight(); }
					else
					{ JumpLeft(); }
					break;
					
					
				case AIState.GoHome:
					GoHome();
					break;
					
				default:
					StopLeft();
					break;
				}
			}
			// Apply Gravity
			velocity.y -= gravity * Time.deltaTime;
			
			// move the controller
			controller.Move(velocity * Time.deltaTime);
		}
		if(m_CurrentState == AIState.EnemyDie)
		{
			if(isRight)
			{ 	Debug.Log("Dying Right");
				DieRight(); }
			else
			{ 	Debug.Log("Dying Left");
						DieLeft(); }
		}
	}
	
	private IEnumerator randomJump()
	{
	
		while(enabled)
		{
			float num = Random.Range(0.0f,3.0f);
			yield return new WaitForSeconds(num);
			
			m_CurrentState = AIState.Jump;
		}
			
		
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "pathnode")
		{
			PathNode link = col.GetComponent<PathNode>();
			Debug.Log("PathNode enum: "+link.m_PathInstruction);
			m_CurrentState = link.m_PathInstruction;
		
		}
		
		if(col.tag == "keyboard")
		{
			Debug.Log("Hit the Keyboard",this);
			GameObject playerLink;
			playerLink = GameObject.Find("Player");
			playerLink.GetComponent<move>().moveDirection.y += m_DeathForce;
			m_CurrentState = AIState.EnemyDie;
			Debug.Log(m_CurrentState);
			Debug.Log(controller.isGrounded);
		}
		
		if(col.tag == "enemy")
		{
			if(col.collider != collider)
			{
				Physics.IgnoreCollision(col.collider,collider);
			}
		}
	}
	
	void PatrolRight()
	{
		velocity.x = m_MoveSpeed * Time.deltaTime;
		isRight = true;
	}
	
	void PatrolLeft()
	{
		velocity.x = -m_MoveSpeed * Time.deltaTime;
		isRight = false;
		// play thru sprite sheet: aniPlay.animsprite bla bla bla
	}
	
	void StopRight()
	{
		velocity.x = 0;
		isRight = true;
	}
	
	void StopLeft()
	{
		velocity.x = 0;
		isRight = false;
	}
	
	void JumpRight()
	{
		velocity.y = m_JumpSpeed;
		// play jump animation
		isRight = true;
	}
	
	void JumpLeft()
	{
		velocity.y = m_JumpSpeed;
		isRight = false;
	}
	
	void DieRight()
	{
		//velocity.x = 0;
		// yield new WaitForSeconds(#of animation);
		// play death animation
		Debug.Log("Dying Right");
		Destroy(gameObject);
	}
	
	void DieLeft()
	{
		//velocity.x = 0;
		Debug.Log("Dying Left");
		Destroy(gameObject);
	}
	
	void ChasePlayer()
	{
		if(transform.position.x <= m_ChaseTarget.position.x)
		{
			m_CurrentState = AIState.MoveRight;
		}
		if(transform.position.x > m_ChaseTarget.position.x)
		{
			m_CurrentState = AIState.MoveLeft;
		}
	}
	
	void GoHome()
	{
		if(transform.position.x <= m_HomeTarget.position.x)
		{
			m_CurrentState = AIState.MoveRight;	
		}
		if(transform.position.x > m_HomeTarget.position.x)
		{
			m_CurrentState = AIState.MoveLeft;
		}
		
	}
	
	// toggle Gizmo for the designers to see the range
	void OnDrawGizmos()
	{
		if(m_GizmoToggle)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(transform.position,m_AttackRange);
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(transform.position,m_SearchRange);
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(m_HomeTarget.position, m_ReturnHomeRange);
		}
		
	}
	
}

