using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	
	public float MOVEMENTSPEED = 6.0f;
	public float speed = 6.0f;
	public float jumpspeed = 8.0f;
	public float gravity = 20.0f;
	public float invuln = 0f;
	public float INVULN_LEN = 10f;
	public int m_Hp = 5;
	private int MAX_HP = 5;
	public int m_Lives = 5;
	private int MAX_LIVES = 5;
	
	public Vector3 moveDirection = Vector3.zero;
	
	//GameObjects
	private GameObject objCamera;
	private GameObject objPlayer;
	private GameObject objSpawn;
	private VariableScript ptrScriptVariable;
	
	// Use this for initialization
	void Start () {
		objCamera = (GameObject) GameObject.FindWithTag("MainCamera");
		objPlayer = (GameObject) GameObject.FindWithTag("Player");	//change this
		objSpawn = (GameObject) GameObject.Find("SpawnPoint");
		ptrScriptVariable = (VariableScript) objPlayer.GetComponent( typeof(VariableScript) ); //change this
	}
	
	// Update is called once per frame
	void Update () 
	{
		HandleInvulnerability();
		CharacterController controller = GetComponent<CharacterController>();
		
			if(controller.isGrounded){
				moveDirection = new Vector3(Input.GetAxis("Horizontal"),0,0);
				moveDirection = transform.TransformDirection(moveDirection);
				moveDirection *= speed;
				
				if( Input.GetButton("Jump")){
					moveDirection.y = jumpspeed;
				}
				// Fire1 should be changed to Run later - useful for when you are 
				// holding down the sprint button
				if(Input.GetButton("Jump")&& Input.GetButton("Fire1")){
					moveDirection.y = jumpspeed + 5;
				}
			}
			if(!controller.isGrounded){
				moveDirection.x = Input.GetAxis("Horizontal");
				moveDirection.x *= speed;
			}
			
				
			moveDirection.y -= gravity * Time.deltaTime;
			controller.Move(moveDirection*Time.deltaTime);
		
		
		//
		
		HandleCamera();
		if( Input.GetButtonDown("Fire1") ){
			HandleProjectiles();	
		}
		HandleHealth();
		
		
		
	}
	
	void HandleCamera(){
		objCamera.transform.position = new Vector3 (transform.position.x,transform.position.y,-5);	
	}
	
	void HandleProjectiles(){
		GameObject objCreatedBullet = (GameObject) Instantiate(ptrScriptVariable.objBullet, transform.position, Quaternion.identity);
		Physics.IgnoreCollision(objCreatedBullet.collider, collider);
	}
	
	void HandleInvulnerability(){
		invuln--;
		if( invuln <= 0 ){
			invuln = 0;
			//speed = MOVEMENTSPEED;
		}
		else{	// it is hit, so it is invulnerable
			//speed = 0;
		}
	}
	
	void HandleHealth(){
		if(m_Hp <= 0){
			//Destroy(gameObject);	
			m_Hp = MAX_HP;
			Debug.Log("LOST A LIFE");
			invuln = INVULN_LEN;
			HandleLives();
			
		}
	}
	
	void HandleLives(){
		if(m_Lives > 0){
			m_Lives--;
		}
		else{
			transform.position = new Vector3(objSpawn.transform.position.x, objSpawn.transform.position.y,0);
			Debug.Log("RESTART");
			m_Lives = MAX_LIVES;
		}
		
	}
	
	/*bool isHit(){
		if( invuln > 0){
			return true;	
		}
		else{
			return false;
		}
	}*/
}
