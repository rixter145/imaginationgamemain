using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {
	
	private float moveSpeed = 10f; // how fast the bullet moves
	private float timeSpentAlive; // how long the bullet has stayed alive for
	private GameObject objPlayer;
	private Vector3 tempVector;

	// Use this for initialization
	void Start () {
		objPlayer = (GameObject) GameObject.FindWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
		timeSpentAlive += Time.deltaTime;
		if (timeSpentAlive > 2) // if we have been traveling for more than two seconds remove the bullet
		{
			removeMe();
		}
		// move the bullet
		transform.Translate( Vector3.right * moveSpeed * Time.deltaTime);
	}
	void removeMe()
	{
		Destroy(gameObject);
	}
		
	void OnCollisionEnter(Collision Other)
	{
		EnemyScript health = (EnemyScript) Other.gameObject.GetComponent( typeof(EnemyScript) );
		if ( health != null && Other.gameObject != objPlayer ){ // if we have hit a character and it is not the player
			Debug.Log("HIT");
			health.m_Hp--;
		}
		removeMe(); // remove the bullet if it has hit something else apart from an enemy character

		
		
	
	}
}