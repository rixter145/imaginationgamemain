using UnityEngine;
using System.Collections;
	


public class PathNode : MonoBehaviour
{


	public bool m_OverrideJump;
	public float m_JumpOverride = 8.0f;
	public float m_XSpeed = 10.0f;
	public float m_YSpeed = 10.0f;
	
	public bool m_ChangeInstructionTo = false;
	public bool m_RemoveOnTrigger=false;
	
	public AIState m_PathInstruction;
	
	public AIState m_Change;
	
	public int m_TriggerCountDown = 2;
	private float removeCountDown = 1.0f;
	
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "enemy")
		{
			if(m_ChangeInstructionTo)
			{
				if(m_TriggerCountDown<=0)
				{
					if(col.GetComponent<AIScript>().m_CurrentState == AIState.RemoveTrigger)
					{
						Destroy(gameObject);
					}
					else
					{
						m_PathInstruction = col.GetComponent<AIScript>().m_CurrentState;
					}
				}
				else
				{
					m_TriggerCountDown--;	
				}
			}
			if(m_RemoveOnTrigger)
			{
				Destroy(gameObject, removeCountDown);
			}
			
		}
	}
	
}
