using UnityEngine;
using System.Collections;

public class FloatingLantern : MonoBehaviour
{
	public float m_MoveSpeed = 20.0f; // speed he moves at
	public float m_SearchRange = 10.0f;
	public float m_XSpeed = 10.0f;
	public float m_YSpeed = 10.0f;
	
	public Transform m_ChaseTarget;
	
	public bool m_LeftDown = false;
	public bool m_RightDown = false;
	public bool m_Circle = false;
	public bool m_Sin = false;
	public float m_RotateAmount = 0.0f;
	
	private Vector3 velocity = Vector3.zero;
	private Vector3 updatedVector;
	
	private CharacterController controller;
	
	public bool m_GizmoToggle = false;
	
	void Start()
	{
		controller = GetComponent<CharacterController>();
		updatedVector = new Vector3(1,0,1);
	}
	
	void Update()
	{
		if(m_LeftDown || m_RightDown)
			MeteorAI();
		if(m_Circle)
			CircleAI(m_RotateAmount);
		if(m_Sin)
			SinAI(m_RotateAmount);
		else
			Debug.Log("You haven't chosen an AI behaviour",this);
	}
	
	
	
	void moveLeftDown()
	{
		velocity.x = -m_XSpeed;
		velocity.y = -m_YSpeed;
		controller.Move(velocity * Time.deltaTime);
	
	}
	
	void moveRightDown()
	{
		velocity.x = m_XSpeed;
		velocity.y = -m_YSpeed;
		controller.Move(velocity * Time.deltaTime);
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			//dmg the player
		}
		if(col.tag == "keyboard")
		{
			// play death animation
			Destroy(gameObject);
		}
		if(col.tag == "mainfloor")
		{
			Destroy(gameObject);
		}
		else
		{
			if(col.collider != collider)
			{
				Physics.IgnoreCollision(col.collider,collider);
			}
		}
		
	}
	
	void MeteorAI()
	{
		float distanceToTarget = Vector3.Distance(m_ChaseTarget.position,transform.position);
		
		if(distanceToTarget > m_SearchRange)
		{
			return;
		}
		
		if(m_LeftDown)
		{
			moveLeftDown();
			
		}
		else
		{
			moveRightDown();
		}
	}
	
	void CircleAI(float r)
	{
		updatedVector =(Quaternion.Euler(0,1/r,0) * updatedVector);
		controller.Move(updatedVector * Time.deltaTime * m_MoveSpeed);
	}
	
	void SinAI(float r)
	{
		updatedVector = (Quaternion.Euler(1/r,0,0)*updatedVector);
		controller.Move(updatedVector*Time.deltaTime*m_MoveSpeed);
	}
	
	void OnDrawGizmos()
	{
		if(m_GizmoToggle)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(transform.position,m_SearchRange);
		}
	}
	
}
