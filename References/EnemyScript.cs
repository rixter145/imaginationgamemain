using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
	
	private GameObject objPlayer;
	private Vector3 knockback;
	public float knockMagnitude = 100f;
	public int m_Hp = 5;
	//public float invuln_len = 5f;
	
	// Use this for initialization
	void Start () {
		objPlayer = (GameObject) GameObject.FindWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
		if( Vector3.Distance(objPlayer.transform.position,transform.position) <1.2f  ){
			PlayerScript inv = (PlayerScript) objPlayer.gameObject.GetComponent( typeof(PlayerScript) );
			if(inv.invuln <= 0){
				Debug.Log("OUCH");
				PlayerScript health = (PlayerScript) objPlayer.gameObject.GetComponent( typeof(PlayerScript) );
				health.m_Hp--;
				HandleHealth();
			}
		}
		

	}
	
	void HandleHealth(){
		if(m_Hp <= 0){
			Destroy(gameObject);	
		}
	}
	
	/*void OnTriggerEnter(Collider col){
		if(col.tag == "Player")
		{
			PlayerScript inv = (PlayerScript) objPlayer.gameObject.GetComponent( typeof(PlayerScript) );
			if(inv.invuln <= 0){
				Debug.Log("OUCH",this);
			}
		}
	}*/
	
	/*void OnCollisionEnter( Collision Other ){
		Debug.Log(Other);
		if( Other.gameObject == objPlayer ){
			Debug.Log("OUCH");
			HealthScript health = (HealthScript) Other.gameObject.GetComponent( typeof(HealthScript) );
			health.m_Hp--;
			
			knockback = new Vector3( (transform.position.x - Other.transform.position.x), (transform.position.y - Other.transform.position.y), 0);
			Other.transform.Translate(knockback * knockMagnitude * Time.deltaTime);
		}
	}*/
}
