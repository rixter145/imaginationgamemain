public enum AIState
	{
		MoveLeft,
		MoveRight,
		Idle,
		Jump,
		EnemyDie,
		GoHome,
		RemoveTrigger
	}